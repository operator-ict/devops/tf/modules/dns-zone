variable "name" {
  type = string
}

variable "parrent_zone_name" {
  type = string
}

variable "resource_group_name" {
  type = string
}
