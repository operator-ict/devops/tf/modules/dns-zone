terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">= 4.2.0, < 5.0.0"
    }
  }
}

resource "azurerm_dns_zone" "zone" {
  name                = var.name
  resource_group_name = var.resource_group_name
}

resource "azurerm_dns_ns_record" "parrentNsRecord" {
  count               = length(var.parrent_zone_name) > 1 ? 1 : 0
  name                = substr(var.name, 0, length(var.name)-length(var.parrent_zone_name)-1 )
  zone_name           = var.parrent_zone_name
  resource_group_name = var.resource_group_name
  ttl                 = 3600

  records = azurerm_dns_zone.zone.name_servers
}
